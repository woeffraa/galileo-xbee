# Galileo XBee

## Abstract
This library make simplest the usage of the XBee shield for a Intel Galileo board.

## Installation on Arduino IDE
1. Past this folder on <path\_to\_ide_installation\>/libraries

### Important :
2. Remove the .git folder (probably hidden).
3. Remove this REAMDE.md file.

These steps are because the arduino ide only supports a specified folder architecture.

It is also possible to link the src on the lib with the git folder (Windows : use mklink and restart the IDE).

## Generation of the documentation

1. With [Doxygen](http://www.stack.nl/~dimitri/doxygen/download.html "Doxygen"), run the configuration on the *./extras/Doxyfile* file.