#ifndef XBEE_H
#define XBEE_H

#include <Arduino.h>

#define XBEE_BAUDRATE 9600

/*!
\mainpage Welcome
Welcome to the reference of the \b %GalileoXBee Library!
This library make simplest the usage of the XBee shield on the Intel Galileo board.
\n
The already instantiated XBeeClass called XBee should be used to call the functions.
<p>
	Call Serial1(9600) to initialize the serial communication at 9600 bauds with the modem. <br>
	Beetween XBee.startCommand() and XBee.endCommand(), all other functions can be used to configure the XBee modem.
	After that, use normally the Serial1 object.
</p>
*/

/**
 * This class offers high-level function to configure an XBee device using the XBee shield for Arduino
 * with an Intel Galileo Board.
 */
class XBeeClass
{
  public:
  XBeeClass();
	
	
	/**
		Enter on the setting mode. The setting mode should be correctly exited using the endCommand() method. 
		Otherwise, the setting mode will be finished automatically but no transmission will be possible during many seconds.
		@see endCommand()
		@return True if correctly entered on the setting mode.
	*/
  boolean startCommand();
	
	/**
		Exit the setting mode.
		@see startCommand()
		@return True if correctly exited from the setting mode.
	*/
  boolean endCommand();
	
	/**
		Set the label of the device.
		@param name The label
	*/
  boolean setLabel( String name );
	
	/**
		Get the label of the device.
		@return The label
	*/
  String getLabel();
	
	/**
		Set the mask of the network where the device will try to connect.
		@param id Network's mask
		@return True if correctly set
	*/
  boolean setNetId( int id );
	
	/**
		Get the current connected network.
		@return The network id
	*/
  int getNetId();
	
	/**
		Get the 16 chars (max.) of the device's serial number on the following format : "<high> <low>".
		This corresponds on 32 bits in hexadecimal.
		@return Serial number
	*/
  String getSerialNumber();
	
	/**
		Set the serial number of the other device which will communicate this device.
		@return True if correctly set
		@param high Firsts 32 bits
		@param low Lasts 32 bits
	*/
  boolean setDestinationAddress(int high, int low);
  
  private:
	
	/**
		Read the serial port as long as data are available and get the result as a String.
		@return Read data.
	*/
  String readResponse();

  /**
      Get the 8 last chars of the device's serial number.
      This corresponds on 16 bits in hexadecimal.
      @return 8 last chars of serial number
  */
  String getSerialNumberLow();

  /**
      Get the 8 first chars of the device's serial number
      This corresponds on 16 bits in hexadecimal.
      @return 8 first chars of serial number
  */
  String getSerialNumberHigh();
};

/**
 * Singleton instance of the XBeeClass
 */
extern XBeeClass XBee;

#endif // XBEE_H






