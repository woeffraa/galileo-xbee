#include "XBee.h"

XBeeClass::XBeeClass(){
   // begin( XBEE_BAUDRATE );
   // Serial1.begin( 9600 );
}

/*String XBeeSettings( String name, int dH, int dL ){
	XBee XBee;
	
	if(XBee.startCommand()){
		XBee.setLabel( name );
		XBee.setDestinationAddress( dH, dL );
		
		String str = "\nName    : ";
		str += String( XBee.getLabel() );
		str += "\nNetwork : ";
		str += String( XBee.getNetId() );
		str += "\nAddress : ";
		str += XBee.getSerialNumber();

		XBee.endCommand();
		
		return str;
	}
	
	return "Cannot connect to the XBee module";
}*/

String XBeeClass::readResponse(){
  delay(50);
  String response = String("");
  while( Serial1.available() > 0 ) {
    response += (char) Serial1.read();
  }
  return response;  
}

boolean XBeeClass::startCommand(){
  Serial1.print("+++");
  delay(1100);
  return readResponse().equals("OK\r");
}

boolean XBeeClass::endCommand(){
  Serial1.println("ATCN");
  return readResponse().equals("OK\r");
}

boolean XBeeClass::setLabel( String name ){
  Serial1.print("ATNI");
  Serial1.print(name);
  Serial1.print('\r');
  return readResponse().equals("OK\r");
}

String XBeeClass::getLabel(){
  Serial1.print("ATNI\r");
  return readResponse();
}

boolean XBeeClass::setNetId( int id ){
  Serial1.print("ATID");
  Serial1.print(id);
  Serial1.print('\r');
  return readResponse().equals("OK\r");
}

int XBeeClass::getNetId(){
  Serial1.print("ATOP\r");
  return readResponse().toInt();  
}

String XBeeClass::getSerialNumber(){
  String response = getSerialNumberHigh();
  response += " ";
  response += getSerialNumberLow();
  return response;
}

String XBeeClass::getSerialNumberHigh(){
  Serial1.print("ATSH\r");
  return readResponse();
}

String XBeeClass::getSerialNumberLow(){
  Serial1.print("ATSL\r");
  return readResponse();
}

boolean XBeeClass::setDestinationAddress(int high, int low){
  boolean readOK = true;
  Serial1.print("ATDH");
  Serial1.print(String(high, HEX));
  Serial1.print('\r');
	readOK = readOK & readResponse().equals("OK\r");
  Serial1.print("ATDL");
  Serial1.print(String(low, HEX));
  Serial1.print('\r');
	readOK = readOK & readResponse().equals("OK\r");

  return readOK;
}

XBeeClass XBee;

